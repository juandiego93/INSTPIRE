﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using EO.WebBrowser;
using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Web.Script.Serialization;
using System.IO;
using unirest_net.http;
using System.Web;

namespace ScraperInstagram
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                wc1.WebView.CertificateError += OnCertificateError;
                wc1.WebView.Url = "https://www.instagram.com/stylelovelyweb";
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        private void OnCertificateError(object sender, CertificateErrorEventArgs e)
        {
            e.Continue();

        }

        async Task PutTaskDelay()
        {
            await Task.Delay(150);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //Cadena de Conexión
            string connectionString = "datasource=localhost;port=3306;username=root;password=;database=mydb;SslMode=none";

            //Consulta para extraer datos de influencers
            string queryUrls = "SELECT * FROM ti_influencers";

            MySqlConnection databaseConnection1 = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase1 = new MySqlCommand(queryUrls, databaseConnection1);
            commandDatabase1.CommandTimeout = 60;
            MySqlDataReader reader1 = null;

            try
            {
                databaseConnection1.Open();
                reader1 = commandDatabase1.ExecuteReader();
            }
            catch (Exception ex)
            {
                databaseConnection1.Close();
                MessageBox.Show(ex.Message);
            }

            //Ciclo para leer consulta anterior
            while (reader1.Read())
            {
                string url2 = "https://www.instagram.com";
                string url = String.Format("{0}", reader1["url"]);

                var htmlDoc = new HtmlAgilityPack.HtmlDocument();

                //Entra a la pagina del link
                wc1.WebView.LoadUrlAndWait(url);

                // AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII

                //  HttpRequest request = Unirest.get(url);
                //HttpRequest request = Unirest.post(url);

                HttpResponse<string> response1 = Unirest.Post("https://deepsocialapi.com/v1/sampling_request?api_token=cblwar6u5008cww&url=https://www.instagram.com/meeeeeeeel_/")

            .header("X-Mashape-Key", "xxx")
            .header("Accept", "application/json") 
            .asJson<string>();

                var b = response1.Body;
                          Console.WriteLine(b); 

                //for (var i = 0; i < 100000; i++)
                //{

                //    wc1.WebView.SetScrollOffset(0, i);
                //    Console.WriteLine(i); 
                //    await PutTaskDelay();

                //    i += 5000;
                //}

                //wc1.WebView.SetScrollOffset(0, 0);

                // AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII

                wc1.WebView.Engine.CookieManager.DeleteCookies(); 

                wc1.WebView.SetScrollOffset(0, 0);

                //Obtener el DOM
                var pagina = wc1.WebView.GetHtml();
                 
                Console.WriteLine(pagina);

                htmlDoc.LoadHtml(pagina);
                 
                //Variables para la cuenta y las publicaciones
                string cuentaPrivada = null;
                string sinPublicaciones = null;
                string cuentaNoDisponible = null;

                //Validacion si la cuenta es privada
                try
                {
                    try
                    {
                        cuentaNoDisponible = htmlDoc.DocumentNode.SelectSingleNode("/html/body/div/div[1]/div/div/h2").InnerText;
                        if (cuentaNoDisponible != null)
                        {
                            //Se inserta influencer que no tiene pagina disponible
                            string usuarioNoDisponible = String.Format("{0}", reader1["nombre"]);
                            insertarUsuarioSQL(usuarioNoDisponible, usuarioNoDisponible, "Página no disponible", "Página no disponible", "Página no disponible", "Página no disponible", "Página no disponible", "Página no disponible");
                        }
                    }
                    catch
                    {
                        try
                        {
                            cuentaPrivada = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div/div/h2").InnerText;
                            if (cuentaPrivada != null)
                            {
                                //Se inserta influencer que no tiene pagina disponible
                                string usuarioPrivado = String.Format("{0}", reader1["nombre"]);
                                insertarUsuarioSQL(usuarioPrivado, usuarioPrivado, "Usuario Privado", "Usuario Privado", "Usuario Privado", "Usuario Privado", "Usuario Privado", "Usuario Privado");
                            }
                        }
                        catch
                        {
                            sinPublicaciones = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div[2]/article/div/div/div[2]/h1").InnerText;
                        }
                    }

                }
                catch
                {
                    pagina = wc1.WebView.GetHtml();
                    htmlDoc.LoadHtml(pagina);

                    string patron = "\\'";
                    string remplazar = " ";

                    //Validacion de Biografia
                    string biografia = "";

                    try
                    {
                        biografia = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/div[2]/span[1]").InnerText;
                    }
                    catch
                    {
                        biografia = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/div[2]/span[1]").InnerText;
                    }

                    //Consumo de la API
                    var client = new RestClient("http://api.meaningcloud.com/topics-2.0");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");

                    //Llamado a la API, respuesta en JSON
                    request.AddParameter("application/x-www-form-urlencoded", "key=624808b5a9346d301fa1c0d4b7eab748&lang=en&txt=??" + biografia + "&&tt=e", ParameterType.RequestBody);

                    //Llamado a la API, respuesta en XML
                    //request.AddParameter("application/x-www-form-urlencoded", "key=624808b5a9346d301fa1c0d4b7eab748&lang=en&txt=??"+biografia+"&doc='XML'&tt=e", ParameterType.RequestBody);

                    //Llamado a la API, respuesta en HTML
                    //request.AddParameter("application/x-www-form-urlencoded", "key=624808b5a9346d301fa1c0d4b7eab748&lang=en&txt=??" + biografia + "&of=xml&tt=e", ParameterType.RequestBody);

                    //Respuesta de la API
                    IRestResponse response = client.Execute(request);

                    //Variable dinamica para recibir respuesta API
                    dynamic jsonResponse = JObject.Parse(response.Content);

                    //Variable de ciudad de usuario
                    string ciudadOrigen = "";

                    //Variable para guardar las entidades del JSON
                    var varXml = jsonResponse.entity_list;

                    //Contador para recorrer entidades del JSON
                    int i = 0;

                    //Ciclo para extraer la ciudad o pais del usuario
                    while (i < varXml.Count)
                    {

                        //Tipo de entidad recorrida
                        string typeJ = varXml[i].sementity.type;

                        //Si la entidad contiene el valor de tipo Location
                        if (typeJ.Contains("Top>Location>GeoPoliticalEntity"))
                        {
                            ciudadOrigen = (varXml[i].form);
                        }
                        else
                        {
                            ciudadOrigen = "No hay País/Ciudad Referenciado";
                        }

                        i++;
                    }

                    Regex rgx = new Regex(patron);

                    string biografia2 = rgx.Replace(biografia, remplazar);

                    //Atributos Usuario 
                    string usuario = htmlDoc.DocumentNode.SelectSingleNode("//h1[@class='AC5d8 notranslate']").InnerText;
                    string nombreUsuario = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/div[2]/h1").InnerText;
                    string numeroPublicaciones = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/ul/li[1]/span/span").InnerText;
                    var publicaciones = htmlDoc.DocumentNode.SelectNodes("//div[@class='Nnq7C weEfm']/div/a");
                    string cadenaComentarios = htmlDoc.DocumentNode.SelectSingleNode("/html/head/meta[11]").Attributes["content"].Value;
                    string numeroSeguidores = "";
                    string numroSeguidos = "";
                    string fechaPublicacion = "";
                    string autorPublicacion = "";
                    string fotoPerfil = "";

                    //Validacion para Foto de perfil segun la cuenta  
                    try
                    {
                        fotoPerfil = htmlDoc.DocumentNode.SelectSingleNode("/html[1]/body[1]/span[1]/section[1]/main[1]/div[1]/header[1]/div[1]/div[1]/span[1]/img[1]/@src[1]").Attributes["src"].Value; 
                    }
                    catch
                    {
                        fotoPerfil =  "Foto de perfil no encontrada"; 
                    }

                    //Validacion para numero de Seguidores
                    try
                    {
                        numeroSeguidores = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/ul/li[2]/a/span").Attributes["title"].Value;
                    }
                    catch
                    {
                        try
                        {
                            numeroSeguidores = htmlDoc.DocumentNode.SelectSingleNode("//li[@class='Y8-fY '][2]/a/span").Attributes["title"].Value;
                        }
                        catch
                        {
                            numeroSeguidores = "No se ha detectado";
                        }
                    }

                    //Validacion para numero de Seguidos
                    try
                    {
                        numroSeguidos = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/header/section/ul/li[3]/a/span").InnerText;
                    }
                    catch
                    {
                        try
                        {
                            numroSeguidos = htmlDoc.DocumentNode.SelectSingleNode("//ul[@class='k9GMp ']/li[3]/a/span").InnerText;
                        }
                        catch
                        {
                            numroSeguidos = "No se encontro etiqueta";
                        }
                    }

                    //Funcion para obtener numero de comentarios 
                    string obtenerNumeroComentarios()
                    {
                        //Variables de comparacion de cadenas 
                        string cadena = htmlDoc.DocumentNode.SelectSingleNode("/html/head/meta[11]").Attributes["content"].Value;
                        string stringInicial = ", ";
                        string stringFinal = " Comments";
                        int terminaString = cadena.LastIndexOf(stringFinal);
                        string nuevoString = cadena.Substring(0, terminaString);
                        int offset = stringInicial.Length;
                        int iniciaString = nuevoString.LastIndexOf(stringInicial) + offset;
                        int cortar = nuevoString.Length - iniciaString;
                        nuevoString = nuevoString.Substring(iniciaString, cortar);
                        return nuevoString;
                    }

                    //Iniciacion de variables de la publicacion 
                    string urlPublicacion = "";
                    string likes = "";
                    string reproducciones = "";
                    string ubicacion = "";
                    string textoPublicacion = "";
                    //Variable atributo para capturar si es foto o video 
                    string attbtoPublicacion = "";

                    //Contador para comparar cantidad de publicaciones 
                    int contadorPublic = 0;

                    //Variable para cargar pagina
                    string pagina2 = "";

                    //Funcion para insertar usuario
                    insertarUsuarioSQL(usuario, nombreUsuario, fotoPerfil, numeroPublicaciones, numeroSeguidores, numroSeguidos, biografia, ciudadOrigen);

                    //Funcion para buscar el ultimo usuario insertado
                    string consultaUltimoRegistro()
                    {
                        //Construccion de insercion SQL    
                        string queryUltimoRegistro = "SELECT * FROM tus_usuarios ORDER BY tus_usuario DESC LIMIT 1";

                        MySqlConnection databaseConnectionUltimo = new MySqlConnection(connectionString);
                        MySqlCommand commandDatabaseUltimo = new MySqlCommand(queryUltimoRegistro, databaseConnectionUltimo);
                        commandDatabaseUltimo.CommandTimeout = 60;
                        MySqlDataReader ultimoRegistro1 = null;

                        try
                        {
                            databaseConnectionUltimo.Open();
                            ultimoRegistro1 = commandDatabaseUltimo.ExecuteReader();
                        }
                        catch (Exception ex)
                        {
                            databaseConnectionUltimo.Close();
                            MessageBox.Show(ex.Message);
                        }

                        //Variable para consultar ultimo usuario ingresado
                        string ultimoUsuarioQ = "";

                        while (ultimoRegistro1.Read())
                        {
                            ultimoUsuarioQ = String.Format("{0}", ultimoRegistro1["tus_usuario"]);
                        }

                        databaseConnectionUltimo.Close();
                        return ultimoUsuarioQ;
                    }

                    if (publicaciones != null)
                    { 
                        //Recorrido de cada publicacion 
                        while (contadorPublic < publicaciones.Count)
                        {
                            htmlDoc.LoadHtml(pagina);

                            //Atributos de cada publicacion
                            urlPublicacion = publicaciones[contadorPublic].Attributes["href"].Value;
                            wc1.WebView.LoadUrlAndWait(url2 + urlPublicacion); 
                            //Carga de la pagina
                            pagina2 = wc1.WebView.GetHtml();
                            htmlDoc.LoadHtml(pagina2);

                            // funcion para mover el scroll
                            wc1.WebView.SetScrollOffset(0, 5000);

                            fechaPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/div[2]/a/time").Attributes["title"].Value; 
                            autorPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/header/div[2]/div[1]/div[1]/a").InnerText;

                            try {
                                textoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/div[1]/ul/li[1]/div/div/div/span").InnerText;
                            }
                            catch {
                                textoPublicacion = "Sin texto en la publicación.";
                            }

                            //Variable para verificar si es foto o video 
                            string tipoPublicacion = "";
                            try {
                                tipoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[1]/div/div/div/div[1]/div/div/video").Attributes["type"].Value;
                            }
                            catch {
                                tipoPublicacion = "Foto";
                            }

                            /*Validacion si es foto, coleccion de fotos o video*/
                            //Si es una foto
                            try
                            {
                                attbtoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[1]/div/div/div[1]/div[1]/img").Attributes["srcset"].Value;
                                likes = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/section[2]/div/span/span").InnerText;
                            }
                            catch (Exception exf)
                            {
                                //Si es una coleccion de fotos
                                try
                                {
                                    attbtoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[1]/div/div/div[1]/div/div[1]/div[1]/img").Attributes["srcset"].Value;
                                    likes = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/section[2]/div/span/span").InnerText;
                                }
                                catch (Exception exf1)
                                {
                                    // Si es video
                                    try
                                    {
                                        attbtoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//video[@class='tWeCl']").Attributes["poster"].Value;
                                        reproducciones = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/section[2]/div/span/span").InnerText;
                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            attbtoPublicacion = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[1]/div/div/div[1]/img").Attributes["src"].Value;
                                            likes = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/div[2]/section[2]/div/span/span").InnerText;
                                        }
                                        catch
                                        {
                                            attbtoPublicacion = "Sin foto";
                                            likes = "Sin Likes";
                                        }
                                    }
                                }

                                try
                                {
                                    ubicacion = "www.instagram.com" + htmlDoc.DocumentNode.SelectSingleNode("//*[@id='react-root']/section/main/div/div/article/header/div[2]/div[2]/a").Attributes["href"].Value;
                                }
                                catch
                                {
                                    ubicacion = "Sin Ubicacion";
                                }
                            }

                            //Variables para funcion obtenerComentarios
                            string inicial = ", ";
                            string final = " ";

                            //Llamado al procedimiento almacenado de Publicaciones de  Usuario
                            insertarPublicaciones(nombreUsuario, urlPublicacion, fechaPublicacion, autorPublicacion,tipoPublicacion, attbtoPublicacion, textoPublicacion, likes, reproducciones, ubicacion, obtenerNumeroComentarios());

                            //aumento de contador para recorrer publicaciones 
                            contadorPublic++;
                        }
                    }
                    else
                    {
                        insertarPublicaciones(nombreUsuario, "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones", "Sin publicaciones");
                    }
                    wc1.WebView.LoadUrlAndWait(url);
                    wc1.WebView.Engine.CookieManager.DeleteCookies();
                }
            }

            //Funcion para llamar procedimiento almacenado: Insertar Usuario
            void insertarUsuarioSQL(string usuario, string nombreUsuario, string fotoPerfil, string numeroPublicaciones, string numeroSeguidores, string numeroSeguidos, string biografia, string ciudadOrigen)
            {
                MySqlConnection insertarUsuarioSQL1 = new MySqlConnection(connectionString);
                try
                {
                    using (MySqlCommand cmG = new MySqlCommand("sp_insercionUsuario", insertarUsuarioSQL1))
                    {
                        insertarUsuarioSQL1.Open();
                        cmG.CommandType = System.Data.CommandType.StoredProcedure; 
                        cmG.Parameters.AddWithValue("_usuario", usuario);
                        cmG.Parameters.AddWithValue("_nombreUsuario", nombreUsuario);
                        cmG.Parameters.AddWithValue("_fotoPerfil", fotoPerfil);
                        cmG.Parameters.AddWithValue("_numeroPublicaciones", numeroPublicaciones);
                        cmG.Parameters.AddWithValue("_numeroSeguidores", numeroSeguidores);
                        cmG.Parameters.AddWithValue("_numroSeguidos", numeroSeguidos);
                        cmG.Parameters.AddWithValue("_biografia", biografia);
                        cmG.Parameters.AddWithValue("_ciudadUsuario", ciudadOrigen);

                        cmG.ExecuteNonQuery();
                    }

                    insertarUsuarioSQL1.Close();
                }
                catch (Exception insertError)
                {
                    insertarUsuarioSQL1.Close();
                    MessageBox.Show(insertError.ToString());
                }

            }


            //Funcion para llamar procedimiento almacenado: Insertar Publicaciones
            void insertarPublicaciones(string nombreUsuario, string urlPublicacion, string fechaPublicacion, string autorPublicacion, string tipoPublicacion,string attbtoPublicacion,string textoPublicacion, string likes, string reproducciones, string ubicacion, string numeroComentarios)
            {
                MySqlConnection insertarPublicacionesSQL = new MySqlConnection(connectionString);
                try
                {
                    using (MySqlCommand cmG = new MySqlCommand("sp_insercionPublicacion", insertarPublicacionesSQL))
                    {
                        insertarPublicacionesSQL.Open();

                        cmG.CommandType = System.Data.CommandType.StoredProcedure;
                        cmG.Parameters.AddWithValue("_nombreUsuario", nombreUsuario);
                        cmG.Parameters.AddWithValue("_urlPublicacion", urlPublicacion);
                        cmG.Parameters.AddWithValue("_fechaPublicacion", fechaPublicacion);
                        cmG.Parameters.AddWithValue("_autorPublicacion", autorPublicacion);
                        cmG.Parameters.AddWithValue("_tipoPublicacion", tipoPublicacion);
                        cmG.Parameters.AddWithValue("_attbtoPublicacion", attbtoPublicacion);
                        cmG.Parameters.AddWithValue("_textoPublicacion", textoPublicacion);
                        cmG.Parameters.AddWithValue("_likes", likes);
                        cmG.Parameters.AddWithValue("_reproducciones", reproducciones);
                        cmG.Parameters.AddWithValue("_ubicacion", ubicacion);
                        cmG.Parameters.AddWithValue("_numeroComentarios", numeroComentarios);

                        cmG.ExecuteNonQuery();
                    }

                    insertarPublicacionesSQL.Close();
                }
                catch (Exception insertError)
                {
                    insertarPublicacionesSQL.Close();
                    MessageBox.Show(insertError.ToString());
                }

            }

        }

        private void wc1_Click(object sender, EventArgs e)
        {

        }
    }

    internal class MyClass
    {
    }
}