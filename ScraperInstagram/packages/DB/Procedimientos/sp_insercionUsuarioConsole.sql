USE `mydb`;
DROP procedure IF EXISTS `sp_insercionUsuarioConsole`;

DELIMITER $$
USE `mydb`$$
CREATE PROCEDURE `sp_insercionUsuarioConsole`(
		_data_extract_date VARCHAR(20),
		_account_name VARCHAR(45), 
        _id VARCHAR(45), 
        _full_name VARCHAR(45),
		_biography VARCHAR(150),
        _profile_external_link VARCHAR(80),
        _profile_photo_link VARCHAR(80), 
        _profile_photo VARCHAR(90),
        _is_private VARCHAR(3),
        _is_verified VARCHAR(3),
        _post_count VARCHAR(12),
        _followers_count VARCHAR(12),
        _following_count VARCHAR(12))
BEGIN
INSERT INTO instagreader(data_extract_date,
						account_name ,
						id, 
						full_name ,
						biography ,
						profile_external_link ,
						profile_photo_link ,
						profile_photo ,
						is_private ,
						is_verified ,
						post_count ,
						followers_count ,
						following_count) 
VALUES (
			_data_extract_date  ,
			_account_name  , 
			_id  , 
			_full_name  ,
			_biography  ,
			_profile_external_link  ,
			_profile_photo_link  , 
			_profile_photo ,
			_is_private  ,
			_is_verified  ,
			_post_count  ,
			_followers_count  ,
			_following_count);
END$$

DELIMITER ;