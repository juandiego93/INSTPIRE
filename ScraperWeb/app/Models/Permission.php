<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Zicaco\Entrust\EntrustRole;

class Permission extends EntrustRole
{
    //Array de atributos de la clase permisos
    protected $fillable = [
    	'name',
    	'display_name',
    	'description'
    ];

    //Funcion para la relacion de muchos a muchos: 
 	//Un permiso -> n roles
 	//Un Roles -> n Permisos
    public function roles(){
    	return $this->belongsToMany('App\Models\Role');
    }

}
