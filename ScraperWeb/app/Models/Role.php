<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Zicaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	//Array de atributos de la clase rol
    protected $fillable = [
    	'name',
    	'display_name',
    	'description'
    ];

    //Funcion para la relacion de muchos a muchos: 
 	//Un usuario -> n roles
 	//Un Roles -> n Usuarios 
    public function users(){
    	return $this->belongsToMany('App\Models\User');
    }
}
