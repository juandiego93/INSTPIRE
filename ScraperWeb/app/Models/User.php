<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;  
use App\Http\Requests\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait; 

class User extends Authenticatable
{
    //Uso de la Clase Trait
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //public $timestamps = false;

    public $timestamp = date ('Ymd G: i: s');

    //Funcion para la relacion de muchos a muchos: 
    //Un usuario -> n roles
    //Un Roles -> n Usuarios
    public function roles(){
        return $this->belongsToMany('App\Models\Role');
    }
}
