/** ----------------------------------------------------------------------------
 *                         Todos los derechos Reservados 

 *  ----------------------------------------------------------------------------
 *	Nombre:        site_script.js
 *	Ruta:         /public/js/Controllers/site_script.js
 *	Descripción:  Script de descripcion empresarial
 *	Fecha:        15/07/2018
 *  Autor:        Juan Diego Osorio Castrillon.
 *  Versión:      1.0 
 *  ----------------------------------------------------------------------------
 *	 							Histórico de cambios
 *  ----------------------------------------------------------------------------
 *	  Fecha           Autor               Descripción
 *  ----------------------------------------------------------------------------
 *  15/07/2018    JuanDiegoOC   Creación del archivo javascript.
 *  ----------------------------------------------------------------------------
 */

/*Variables conjunto de desarrollo*/
var año_Versión = "2018";
var cliente = "";
var desarrollado_por = "";
var desarrollador_Jefe = "Camilo Tavarez";
var desarrolladores = "Mateo Lopez Loaiza \n Juan Diego Osorio Castrillon";
var versión = "Desarrollo";
var PHP = "PHP 7.2 \n Framework Laravel 5.6";
var descripcion_PHP = "->PHP >= 7.2 \n ->https://laravel.com/docs";
var JS = "->jquery 3.0.0 \n ->Bootstrap 3 \n -> ->AngujarJS(1.6.6)";
var CSS = "Bootstrap 3";
